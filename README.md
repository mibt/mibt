# MIBT - Sistema de geração de indicadores para o Mapa Interativo Brasil Transparente

## Requisitos tecnológicos

### Python & Django

* Agilidade de desenvolvimento
    * Linguagem que facilita manutenção e evolução
    * Equipe possui experiencia com a tecnologia
* Fácil aprendizado
    * Comunidade ativa
    * Documentação extensa
    * Organização do Framework

### PostgreSQL

* PostgreSQL tem um melhor tratamento de dados com geolocalização
* Tratamento de datas e horários de maneira correta
* Perde em desempenho por checar gatilhos constantemente
* Mas esses gatilhos o torna mais versátil
 

### Ferramentas de Gerência de Configuração de Software

* Estabilitade da Produção
    * Com integração contínua temos a geração de builds estáveis, de maneira automática e contínua
    * Feedback rápido e automático de builds que falahrem (testes ou analise estática de código

* Agilidade na entrega
    * Após todo o processo de integração contínua, ocorre o processo de deploy contínuo
    * Como critério de entrega, Builds que falham não vão para homologação ou produção

* Pré-configuração do Ambiente Automatizada
    * Com scripts de ferramentas de gerencia de configuração nós podemosautomatizar a instalação de dependencias e configuração do sistema para a instalação do Webapp em si


## Licença

    Copyright (c) the MIBT contributors. See AUTHORS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
