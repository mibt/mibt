from django.db import models
from core.models import MIBTManager


class Ente(models.Model):
    nome = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255,
                            choices=(('es', 'Estado'),
                                     ('mu', 'Municipio')))
    codigo_ibge = models.IntegerField(null=True)
    objects = MIBTManager()

    def __unicode__(self):
        return (self.nome)
