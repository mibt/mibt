from django.forms import ModelForm

from .models import Entity


class EntityForm(ModelForm):

    class Meta:
        model = Entity
        fields = ['name']
