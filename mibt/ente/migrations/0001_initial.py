# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=255)),
                ('tipo', models.CharField(max_length=255, choices=[(b'es', b'Estado'), (b'mu', b'Municipio')])),
                ('codigo_ibge', models.IntegerField(null=True)),
            ],
        ),
    ]
