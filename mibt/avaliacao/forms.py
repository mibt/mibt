from django import forms

from .models import Resposta


class RespostaForm(forms.ModelForm):
    pergunta_id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    #texto = forms.CharField(widget=forms.Textarea(attrs={'required': 'true'}))
    texto = forms.ChoiceField(widget=forms.RadioSelect,
                              choices=(('0', 'Nao',), ('1', 'Sim',)))

    class Meta:
        model = Resposta
        fields = ['texto']
