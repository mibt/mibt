# -*- coding:utf-8 -*-
from django.db import models

from avaliador.models import Usuario
from ente.models import Ente
from metodologia.models import Pergunta, Metodologia


class Avaliacao(models.Model):
    data = models.DateField()
    usuario = models.ForeignKey(Usuario)
    metodologia = models.ForeignKey(Metodologia)
    ente = models.ForeignKey(Ente)

    class Meta:
        verbose_name = "Avaliação"
        verbose_name_plural = "Avaliações"


class Resposta(models.Model):
    pergunta = models.ForeignKey(Pergunta)
    texto = models.TextField(null=True)
    valor = models.IntegerField(null=True)
    avaliacao = models.ForeignKey(Avaliacao)

    timestamp = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Respostas"

    def __unicode__(self):
        return self.pergunta.enunciado + "\t" + self.texto
