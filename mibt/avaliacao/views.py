# -*- coding: utf-8 -*-

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.views.generic import View
from datetime import datetime

from .forms import RespostaForm
from metodologia.models import Composicao, Pergunta
from avaliacao.models import Avaliacao
from metodologia.models import Metodologia
from avaliador.models import Usuario


class BlocoPerguntasView(View):
    
    def get(self, request):
        metodologias = Metodologia.objects.all()
        metodologia_id = metodologias[0].id
        context = RequestContext(request, {
                                            "metodologias": metodologias,
                                            "metodologia_id": metodologia_id})
        return render_to_response('bloco_perguntas.html', context)


class ResultadoCidadaView(View):
    
    def get(self, request):
        return render_to_response('resultado_avaliacao_cidada.html')


class RespostaView(View):
    http_method_names = [u'get', u'post']

    def get(self, request):
        metodologia = request.GET.get('metodologia', None)
        item = request.GET.get('item', None)

        if item:
            item = int(item)
        else:
            item = 0

        if not metodologia:
            messages.error(request, 'Não há metodologia escolhida')
            return redirect(reverse('home'), RequestContext(request))

        if item == 0:
            if request.user.is_anonymous:
                usuario = Usuario.objects.all()[0]
                #TODO: Nao autorizar avaliacoes anonimas
            else:
                usuario = request.user
            avaliacao = Avaliacao(usuario=usuario, data=datetime.now(),
            metodologia_id=int(metodologia), ente_id=1)
                                    #TODO: ente_id está hard coded
        composicao = None
        try:
            composicao = Composicao.objects.filter(
                        metodologia__id=metodologia).order_by('id')[item]
        except IndexError:
            # Questionario finalizado
            #XXX: try/catch sendo usado como logica... ma ideia?
            return redirect('resultado_cidada')
            #TODO: redirecionar para tela de finalizacao de questionario

        resposta_form = RespostaForm(initial={'pergunta_id': composicao.pergunta.id})

        context = RequestContext(request, {'resposta_form': resposta_form,
                                           'composicao': composicao})

        return render_to_response('questionario.html', context)


    def post(self, request):
        context = RequestContext(request)

        form = RespostaForm(request.POST)

        resposta = form.save(commit=False)
        resposta.pergunta_id = request.POST.get('pergunta_id', None)
        resposta.avaliacao_id = 1   # TODO: hard coded
        resposta.timestamp = datetime.now()
        resposta.save()

        metodologia = request.GET.get('metodologia', None)
        try:
            item = int(request.GET.get('item', 0))
        except ValueError:
            item =0
        return redirect(reverse('resposta') +
                        "?metodologia=" + metodologia +
                        "&item=" + str(item+1))
