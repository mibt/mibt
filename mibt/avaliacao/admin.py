# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Resposta, Avaliacao

admin.site.register(Resposta)
admin.site.register(Avaliacao)
