from django.conf.urls import url

from .views import RespostaView, BlocoPerguntasView, ResultadoCidadaView

urlpatterns = [
    url(r'^resposta/', RespostaView.as_view(), name='resposta'),
    url(r'^cidada/perguntas/', BlocoPerguntasView.as_view(), name='perguntas_cidada'),
    url(r'^cidada/resultado/', ResultadoCidadaView.as_view(), name='resultado_cidada'),
]
