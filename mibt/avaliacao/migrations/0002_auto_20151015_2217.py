# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacao', '0001_initial'),
        ('ente', '0001_initial'),
        ('metodologia', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='resposta',
            name='pergunta',
            field=models.ForeignKey(to='metodologia.Pergunta'),
        ),
        migrations.AddField(
            model_name='avaliacao',
            name='ente',
            field=models.ForeignKey(to='ente.Ente'),
        ),
        migrations.AddField(
            model_name='avaliacao',
            name='metodologia',
            field=models.ForeignKey(to='metodologia.Metodologia'),
        ),
        migrations.AddField(
            model_name='avaliacao',
            name='resposta',
            field=models.ForeignKey(to='avaliacao.Resposta'),
        ),
        migrations.AddField(
            model_name='avaliacao',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
