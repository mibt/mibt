# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacao', '0002_auto_20151015_2217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resposta',
            name='texto',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='resposta',
            name='valor',
            field=models.IntegerField(null=True),
        ),
    ]
