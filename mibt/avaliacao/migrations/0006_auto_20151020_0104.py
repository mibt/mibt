# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacao', '0005_auto_20151016_0056'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='avaliacao',
            options={'verbose_name': 'Avalia\xe7\xe3o', 'verbose_name_plural': 'Avalia\xe7\xf5es'},
        ),
        migrations.AlterModelOptions(
            name='resposta',
            options={'verbose_name_plural': 'Respostas'},
        ),
        migrations.RemoveField(
            model_name='avaliacao',
            name='resposta',
        ),
        migrations.AddField(
            model_name='resposta',
            name='avaliacao',
            field=models.ForeignKey(default=1, to='avaliacao.Avaliacao'),
            preserve_default=False,
        ),
    ]
