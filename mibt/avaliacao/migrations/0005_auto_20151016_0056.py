# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacao', '0004_auto_20151016_0056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resposta',
            name='texto',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='resposta',
            name='timestamp',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='resposta',
            name='valor',
            field=models.IntegerField(),
        ),
    ]
