# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacao', '0003_auto_20151016_0055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resposta',
            name='timestamp',
            field=models.DateTimeField(null=True),
        ),
    ]
