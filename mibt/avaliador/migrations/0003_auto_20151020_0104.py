# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliador', '0002_auto_20151015_2219'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='avaliador',
            options={'verbose_name_plural': 'Avaliadores'},
        ),
    ]
