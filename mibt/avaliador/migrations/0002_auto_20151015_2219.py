# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliador', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='avaliador',
            field=models.ForeignKey(to='avaliador.Avaliador', null=True),
        ),
    ]
