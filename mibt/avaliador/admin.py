from django.contrib import admin

from .models import Avaliador, Usuario

admin.site.register(Avaliador)
admin.site.register(Usuario)
