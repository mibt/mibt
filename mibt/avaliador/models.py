# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.db import models


class Avaliador(models.Model):
    nome = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255,
                            choices=(('cidadao', 'Cidadão'),
                                     ('controlador', 'Controlador'),
                                     ('gestor', 'Gestor')))

    class Meta:
        verbose_name_plural = "Avaliadores"

    def __unicode__(self):
        return self.nome + " - " + self.tipo


class Usuario(AbstractUser):
    avaliador = models.ForeignKey(Avaliador, null=True)

    def __unicode__(self):
        return (self.first_name + " " + self.last_name)
