# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliador', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Composicao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('peso', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Indicador',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=255)),
                ('descricao', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Metodologia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=255)),
                ('descricao', models.TextField()),
                ('avaliador', models.ForeignKey(to='avaliador.Avaliador')),
                ('indicadores', models.ManyToManyField(to='metodologia.Indicador', through='metodologia.Composicao')),
            ],
        ),
        migrations.CreateModel(
            name='Pergunta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=255, choices=[(b'booleano', b'Booleano'), (b'descritivo', b'Descritivo'), (b'multipla_escolha', b'M\xc3\xbaltipla Escolha'), (b'numerico', b'Num\xc3\xa9rico')])),
                ('enunciado', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='indicador',
            name='perguntas',
            field=models.ManyToManyField(to='metodologia.Pergunta', through='metodologia.Composicao'),
        ),
        migrations.AddField(
            model_name='composicao',
            name='indicador',
            field=models.ForeignKey(to='metodologia.Indicador'),
        ),
        migrations.AddField(
            model_name='composicao',
            name='metodologia',
            field=models.ForeignKey(to='metodologia.Metodologia'),
        ),
        migrations.AddField(
            model_name='composicao',
            name='pergunta',
            field=models.ForeignKey(to='metodologia.Pergunta'),
        ),
    ]
