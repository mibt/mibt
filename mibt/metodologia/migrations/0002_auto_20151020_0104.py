# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('metodologia', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='composicao',
            options={'verbose_name_plural': 'Composi\xe7\xf5es'},
        ),
        migrations.AlterModelOptions(
            name='indicador',
            options={'verbose_name_plural': 'Indicadores'},
        ),
    ]
