from django.contrib import admin

from .models import Pergunta, Indicador, Metodologia, Composicao

admin.site.register(Pergunta)
admin.site.register(Indicador)
admin.site.register(Metodologia)
admin.site.register(Composicao)
