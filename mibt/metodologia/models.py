# -*- coding: utf-8 -*-
from django.db import models

from avaliador.models import Avaliador


class Pergunta(models.Model):
    tipo = models.CharField(max_length=255,
                            choices=(('booleano', 'Booleano'),
                                     ('descritivo', 'Descritivo'),
                                     ('multipla_escolha', 'Múltipla Escolha'),
                                     ('numerico', 'Numérico')))
    enunciado = models.TextField()

    def __unicode__(self):
        return self.tipo + " - " + self.enunciado


class Indicador(models.Model):
    nome = models.CharField(max_length=255)
    descricao = models.TextField()
    perguntas = models.ManyToManyField(Pergunta, through='Composicao')

    class Meta:
        verbose_name_plural = "Indicadores"

    def __unicode__(self):
        return (self.nome)


class Metodologia(models.Model):
    nome = models.CharField(max_length=255)
    descricao = models.TextField()
    indicadores = models.ManyToManyField(Indicador, through='Composicao')
    avaliador = models.ForeignKey(Avaliador)

    def __unicode__(self):
        return (self.nome)


class Composicao(models.Model):
    # Relacionamento entre Pergunta, Indicador e Metodologia
    peso = models.FloatField()
    pergunta = models.ForeignKey(Pergunta)
    indicador = models.ForeignKey(Indicador)
    metodologia = models.ForeignKey(Metodologia)

    class Meta:
        verbose_name_plural = "Composições"

    def __unicode__(self):
        return (self.metodologia.nome +
                " - Indicador " + str(self.indicador.id) +
                " - Pergunta " + str(self.pergunta.id))
