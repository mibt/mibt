from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import View

from metodologia.models import Metodologia


class HomeView(View):
    http_method_names = [u'get']

    def get(self, request):
        metodologias = Metodologia.objects.all()
        metodologia_id = metodologias[0].id
        context = RequestContext(request, {
                                            "metodologias": metodologias,
                                            "metodologia_id": metodologia_id})
        return render_to_response('home.html', context)
