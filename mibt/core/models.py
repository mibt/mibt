""" Default models for MIBT project
"""
from django.db import models


class MIBTManager(models.Manager):
    """ Default manager for MIBT's models
    """

    # Use this manager class for automatic managers whenever it is the default
    # manager of a model. For more information, check out the link below:
    # https://docs.djangoproject.com/en/1.8/topics/db/managers/#controlling-automatic-manager-types
    use_for_related_fields = True

    def __init__(self, *args, **kwargs):
        super(MIBTManager, self).__init__(*args, **kwargs)

    def get_or_none(self, **kwargs):
        """ Tries to get object using params using params set in **kwargs, if it
        doesn't return an object instance, it will throw an exception that the
        object doesn't exist in the database. The exception will be dealt with
        and it will return None instead.
        """
        try:
            self.get(**kwargs)
        except self.model.DoesNotExist:
            return None
